
<?php session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."vendor/autoload.php"); //THIS IS ABSULETE PATH
use \App\bitm\SEIP105795\Book;
use \App\bitm\SEIP105795\Utility\Utility;
use \App\bitm\SEIP105795\Message\Message;
$obj=new Book();
$books=$obj->index();

?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>List of Books</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
<body>
<h1>List of Book</h1>
<div class="warning">
<?php echo Message::flash();?>
</div>
<div>
	<span>Search /Filter </span>
	<span id="utility">Download as PDF | XL <a href="create.php">Create New</a> </span>
	<select>
		<option>10</option>
		<option>20</option>
		<option>30</option>
		<option>40</option>
		<option>50</option>
	</select>
</div>
<table border="1">
	<thead>
		<tr>
			<th>Sl</th>
			<th>Book Title &dArr;</th>
			<th>Author Namr &dArr;</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	<?php
    $slno=0;
	foreach($books as $book):
        $slno++;
	?>
		<tr>
			<td><?php echo $slno;?></td>
            <td><a href="view.php?id=<?php echo $book['id'];?>"><?php echo $book['bookName'];?></a></td>
			<td><?php echo $book['author'];?></td>
			<td><a href="edit.php?id=<?php echo $book['id']?>"> Edit</a> |
                <form action="delete.php" method="post">
                    <input type="hidden" name="id" value="<?php echo $book['id'];?>">
                    <button type="submit">Delete</button>
                </form>
                |Trash/Recover | Email To Friend
            </td>
		</tr>
		<?php endforeach;?>
	</tbody>
</table>
<div><span>prev 1 | 2 | 3 next </span></div>
</body>
</html>