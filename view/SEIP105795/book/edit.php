<?php
session_start();
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."vendor/autoload.php");
use App\bitm\SEIP105795\Book;
use App\bitm\SEIP105795\Message;
use App\bitm\SEIP105795\Utility;
$obj=new Book();
$theBook=$obj->edit($_GET['id']);
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<h1>Edit an Item</h1>
<form action="update.php" method="post">
    <fieldset>
        <legend>
            Edit Book
        </legend>
        <input type="hidden" name="id" value="<?php echo $theBook->id;?>"/>
        <div>
            <label for="bookName">Book Name</label>
            <input
                type="text" name="bookName"id="bookName" required="required" tabindex="3" value="<?php echo $theBook->bookName;?>"/>
        </div>
        <div>
            <label for="author"> Author</label>
            <input
                type="text" name="author"id="author" required="required" tabindex="3" value="<?php echo $theBook->author;?>"/>
        </div>
        <button type="submit">save</button>
        <button type="submit">save & Add Again</button>
        <input type="reset" value="reset"/>
    </fieldset>
</form>
<a href="index.php">Back to the list</a>
</body>
</html>