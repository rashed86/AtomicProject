<?php
//function  __autoload($className){
//    $fileName= "../../../".str_replace("\\", "/", $className).".php";
//    include_once ($fileName);
//}
//use src\bitm\SEIP105795\Organization;
//$obj=new Organization();
//$obj->index();
?>
<?php
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."AtomicProjectGolamRabbani105795".DIRECTORY_SEPARATOR."vendor/autoload.php");
use \App\bitm\SEIP105795\Organization;
$obj=new Organization();
$_organize=$obj->index();

?>

<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>List of Organization</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<h1>List of Organization</h1>
<div>
    <span>Search /Filter </span>
    <span id="utility">Download as PDF | XL <a href="create.php">Create New</a> </span>
    <select>
        <option>10</option>
        <option>20</option>
        <option>30</option>
        <option>40</option>
        <option>50</option>
    </select>
</div>
<table border="1">
    <thead>
    <tr>
        <th>Sl</th>
        <th>Organization Title &dArr;</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
<?php
foreach($_organize as $organization):
?>
        <tr>
            <td><?php echo $organization['id'];?></td>
            <td><?php echo $organization['organizationName'];?></td>
            <td>View | Delete |Trash/Recover | Email To Friend</td>
        </tr>
<?php endforeach;?>
    </tbody>
</table>
<div><span>prev 1 | 2 | 3 next </span></div>
</body>
</html>