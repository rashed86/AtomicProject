<?php
session_start();
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
use App\bitm\SEIP105795\Birthday;
use App\bitm\SEIP105795\Message\Message;
use App\bitm\SEIP105795\Utility\Utility;
$obj=new Birthday();
$thePerson=$obj->edit($_GET['id']);
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<h1>Edit an Item</h1>
<form action="update.php" method="post">
    <fieldset>
        <legend>
            Edit Birthday
        </legend>
        <input type="hidden" name="id" value="<?php echo $thePerson->id;?>"/>
        <div>
            <label for="personName"> Person Name</label>
            <input
                type="text" name="personName" id="personName" required="required" tabindex="3" value="<?php echo $thePerson->name;?>"/>
        </div><div>
            <label for="birthday"> Birth Day</label>
            <input

                type="text" name="birthday" id="birthday" required="required" tabindex="3" value="<?php echo $thePerson->bdate;?>"/>
        </div><div>
            <button type="submit">save</button>
        <button type="submit">save & Add Again</button>
        <input type="reset" value="reset"/>
    </fieldset>
</form>
<a href="index.php">Back to the list</a>
</body>
</html>