<?php session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
use App\bitm\SEIP105795\Email;
use \App\bitm\SEIP105795\Utility\Utility;

$obj=new Email();
$theEmail=$obj-> edit($_GET['id']);

?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<h1>Edit an Item</h1>
<form action="update.php" method="post">
    <fieldset>
        <legend>
            Edit Email
        </legend>
        <input type="hidden" name="id" value="<?php echo $theEmail->id;?>"/>
        <div>
            <label for="cityName"> Email</label>
            <input
                type="text" name="email" id="email" required="required" tabindex="3" value="<?php echo $theEmail->email;?>"/>
        </div>
        <button type="submit">save</button>
        <button type="submit">save & Add Again</button>
        <input type="reset" value="reset"/>
    </fieldset>
</form>
<a href="index.php">Back to the list</a>
</body>
</html>